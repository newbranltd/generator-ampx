/**
 * Hook the gulp-amphtml-validator to test our home page
 */
const { join } = require('path');
const gulp = require('gulp');
const fs = require('fs-extra');
const template = require('gulp-template');
const inject = require('gulp-inject');
const gulpAmpValidator = require('gulp-amphtml-validator');
// need to get the node-configs here
const config = require('config');
const { sassTask } = require('./sass');
// @to1source add windoze compartible coding
const base = join(__dirname, '..');
/**
 * get the correct data to inject into the AMP page
 */
const getData = callback => {
  // const paths = config.get('paths');
  const ampData = config.get('amp');
  callback(ampData);
  /*
  callback(Object.assign({
    css: fs.readFileSync(join(base, paths.dev, paths.style, 'main.css'), 'utf8')
  }, amp));
  */
};

// inject the stream css files into the amp page
const injectFn = stream => {
  return inject(stream, {
    starttag: '<!-- inject:head:style -->',
    transform: (filePath, file) => {
      return `<style amp-custom>${file.contents.toString('utf8')}</style>`;
    }
  })
};

// Run validator
exports.ampValidator = (page) => {
  return () => {
    return gulp.src(page)
      // Validate the input and attach the validation result to the "amp" property
      // of the file object.
      .pipe(gulpAmpValidator.validate())
      // Print the validation results to the console.
      .pipe(gulpAmpValidator.format())
      // Exit the process with error code (1) if an AMP validation error
      // occurred.
      .pipe(gulpAmpValidator.failAfterError());
      // .pipe(gulpAmpValidator.failAfterWarningOrError());
  };
};

// Grab data then put content into the .amp.html using _.template method
exports.compileAmp = (pages, build) => {
  const paths = config.get('paths');
  const dir = build ? paths.dest : paths.dev;
  return done => {
    getData( props => {
      gulp.src(pages)
        .pipe(injectFn(sassTask()))
        .pipe(template(props))
        .pipe(gulp.dest(dir));
      done();
    });
  };
};
