const gulp = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
// due to the way we only use the stream, sourcemap will not be supported
// const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const config = require('config');
const { join } = require('path');

const paths = config.get('paths');
const base = join(__dirname, '..');

/**
 * 2018-06-06 we don't render the whole thing out as CSS
 * anymore, instead just partial export as a stream to embed in the amp page
 */
exports.sassTask = () => {
    return gulp.src( join(base, paths.app, paths.style, 'main.scss') )
      .pipe( sass({
        includePaths: join(process.cwd(), 'node_modules')
      }).on('error', sass.logError) )
      .pipe(postcss([
        autoprefixer,
        cssnano
      ]));
};
